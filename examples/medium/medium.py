"""
Medium CSV - Wine

This data set provides information related to red and white variants of
the Portuguese "Vinho Verde" wine [1]_.

References
----------
.. [1] https://archive.ics.uci.edu/ml/datasets/Wine+Quality
"""

import io

import pandas as pd
import requests

from src.Search import HillClimbing

url = "https://web.stanford.edu/class/aa228/projects/medium.csv"
s = requests.get(url).content
data = pd.read_csv(io.StringIO(s.decode('utf-8')))

hc = HillClimbing()
hc.search(data, save=True)
