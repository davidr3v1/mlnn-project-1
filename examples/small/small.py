"""
Small CSV - Titanic

This data set provides information on the fate of passengers on the fatal maiden voyage of
the ocean liner ``Titanic'', summarized according to economic status (class), sex, age and
survival [1]_.

Notes
-----
Stanford has discretized the data so that you would only have to deal with discrete variables [2]_.

References
----------
.. [1] https://cran.r-project.org/web/packages/titanic/titanic.pdf
.. [2] https://web.stanford.edu/class/aa228/projects/1/
"""

import io

import pandas as pd
import requests

from Learning import ParameterEstimation
from Model import Model

url = "https://web.stanford.edu/class/aa228/projects/small.csv"
s = requests.get(url).content
data = pd.read_csv(io.StringIO(s.decode('utf-8')))

# Learn model
# hc = HillClimbing()
# model = hc.search(data, save=True)

model = Model({'age': {'fare'}, 'portembarked': set(), 'fare': {'numparentschildren', 'numsiblings'},
               'numparentschildren': {'passengerclass'}, 'passengerclass': set(),
               'sex': {'portembarked', 'fare', 'numsiblings', 'age'}, 'numsiblings': set(),
               'survived': {'numparentschildren', 'age'}})

pe = ParameterEstimation(model, data)
pe.parameters(show=True)
