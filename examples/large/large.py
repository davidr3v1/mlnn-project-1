"""
Insane CSV - Secret dungeons
"""

import io

import pandas as pd
import requests

from Search import HillClimbing

url = "https://web.stanford.edu/class/aa228/projects/large.csv"
s = requests.get(url).content
data = pd.read_csv(io.StringIO(s.decode('utf-8')))

hc = HillClimbing()
hc.search(data, save=True)