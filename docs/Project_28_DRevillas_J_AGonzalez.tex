\documentclass{article}

\usepackage{nips13submit_e,times}
\nipsfinaltrue

\usepackage[utf8]{inputenc}

% adds hyper links inside the generated pdf file
\usepackage[final]{hyperref}
\usepackage{graphicx}
\usepackage{babel}

\usepackage{amsmath}
\usepackage{listings}
\usepackage{hyperref}

\title{P28 - Bayesian Network Learning}
\date{2019-10-15}

\author{
	David Revillas \\
	\texttt{drevillas002@ikasle.ehu.eus} \\
	\And
	Jon Ander González\\
	\texttt{jgonzalez248@ikasle.ehu.eus} \\
}

\definecolor{darkgreen}{rgb}{0,0.5,0}
\definecolor{lightgray}{rgb}{0.95,0.95,0.95}
\definecolor{gray}{rgb}{0.65,0.65,0.65}
\lstset{basicstyle=\footnotesize\ttfamily,
		keywordstyle=\color{darkgreen}\bfseries,
		%identifierstyle=\color{blue},
		commentstyle=\color{gray},
		stringstyle=\ttfamily,
		showstringspaces=false,
		tabsize=2,
		backgroundcolor=\color{lightgray},
		literate=
		{á}{{\'a}}1 {é}{{\'e}}1 {í}{{\'i}}1 {ó}{{\'o}}1 {ú}{{\'u}}1
		{Á}{{\'A}}1 {É}{{\'E}}1 {Í}{{\'I}}1 {Ó}{{\'O}}1 {Ú}{{\'U}}1
		{à}{{\`a}}1 {è}{{\`e}}1 {ì}{{\`i}}1 {ò}{{\`o}}1 {ù}{{\`u}}1
		{À}{{\`A}}1 {È}{{\'E}}1 {Ì}{{\`I}}1 {Ò}{{\`O}}1 {Ù}{{\`U}}1
		{ä}{{\"a}}1 {ë}{{\"e}}1 {ï}{{\"i}}1 {ö}{{\"o}}1 {ü}{{\"u}}1
		{Ä}{{\"A}}1 {Ë}{{\"E}}1 {Ï}{{\"I}}1 {Ö}{{\"O}}1 {Ü}{{\"U}}1
		{â}{{\^a}}1 {ê}{{\^e}}1 {î}{{\^i}}1 {ô}{{\^o}}1 {û}{{\^u}}1
		{Â}{{\^A}}1 {Ê}{{\^E}}1 {Î}{{\^I}}1 {Ô}{{\^O}}1 {Û}{{\^U}}1
		{Ã}{{\~A}}1 {ã}{{\~a}}1 {Õ}{{\~O}}1 {õ}{{\~o}}1
		{œ}{{\oe}}1 {Œ}{{\OE}}1 {æ}{{\ae}}1 {Æ}{{\AE}}1 {ß}{{\ss}}1
		{ű}{{\H{u}}}1 {Ű}{{\H{U}}}1 {ő}{{\H{o}}}1 {Ő}{{\H{O}}}1
		{ç}{{\c c}}1 {Ç}{{\c C}}1 {ø}{{\o}}1 {å}{{\r a}}1 {Å}{{\r A}}1
		{€}{{\euro}}1 {£}{{\pounds}}1 {«}{{\guillemotleft}}1
		{»}{{\guillemotright}}1 {ñ}{{\~n}}1 {Ñ}{{\~N}}1 {¿}{{?`}}1
}


\begin{document}

\maketitle

\begin{abstract}
Bayesian networks (BNs) are probabilistic graphical models that serve to represent probabilistic dependencies between variables in uncertain domains. The implementation of methods for learning and inferring these graphical models from data is not straightforward and can be computationally costly.

The aim of the project is to compare 2 different learning methods: \emph{Score-based Structure Learning} and \emph{Constrain-based Structure Learning}. Given a data-set of binary vectors, the program will output the Bayesian Network learned from data, also visualizing it.
\end{abstract}
\pagenumbering{gobble}
\tableofcontents
\newpage
\pagenumbering{arabic}
\section{Introduction}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\linewidth]{images/wine_best.png}
	\caption{Bayesian network structure from the \emph{Wine} data set. For the next explanation an easier example will be use.}
	\label{fig:wine}
\end{figure}

The best way of explaining the Bayesian networks is with examples. For that we will use the \emph{Titanic} CSV (Figure \ref{fig:tit_best}), a data set that provides discretized information about the passengers of the ocean liner.

We will refer to this graphs taking into account the direction of the arcs. The source node will be referred as being above of the destination node. If a node has other nodes directly above him, they will be called parents. The nodes that do not have parents will be considered as roots.

As shown in Figure \ref{fig:tit_best} every node contains a variable of the passengers. As this is a complex system the variables condition the probabilities of each other. For example, if we take a look a the class of the passengers, we could see that there is correlation between the class and the survival rate (better classes were more likely of getting a rescue boat, and thus, more chances of survival).
On the other way, if we know that they survived, we would be more certain of the age and sex of the passenger.

Even though we could think that  every node of the system is dependent of each other, thanks to the built-in independence assumptions that Bayesian Networks have  we don't need the probabilities of every node to compute the conditional probabilities
 \cite{charniak1991bayesian}. We just need the probabilities of the roots, and the conditional probabilities of the other nodes taking into account their direct parents.
This saves a lot of calculations given that the complete probability distribution for a binary example is $2^n -1$ (given that the probabilities must sum 1, we can ignore one  and infer it from the others).

\subsection{Independence assumptions}

To explain how independece assumptions work will use the Figure \ref{fig:tit_best} as our example again. If we look at the relationship between fare and survived, we would think that they are dependent, since the higher the fare higher the probabilities of survival. But if we knew the age of the passenger, since is the only node between the fare and the survival, we should not need to know the fare to infer if they have survived, and thus, they become conditionally independent given the age. A variable is dependent on other variable given the evidence E, if there is a \emph{d-connecting} path between those variables given E. If a path is not d-connected, we call it  \emph{d-separated}. 

In bayesian networks, thanks to them being DAGs, if we calculate the joint probability of one node following topological order, we can see that we get the probabilities needed for that node to be d-separated to every node except his direct parents. Better explained in  \cite{charniak1991bayesian2}, in the sections \emph{Independece assumptions} and \emph{Consistent probabilities}.

\subsection{Evaluating networks}
There are a certain group of Bayesian networks that can be applied exact solution without it being to costly. This group is the singly connected networks, a graph which has only one path between nodes, ignoring the direction of the edges.
Since the examples we are working with can not be assumed to be singly connected, we can not afford to compute the exact solution, thus,
the approach that must be taken while using our networks for evaluation is getting approximations. \cite{charniak1991bayesian}


\section{Structure Learning}

\subsection{Score Based Structure Learning}

\subsubsection{Score metrics}

There exist many score metrics available. We decide to use the Minimum Description Length (MDL), which  aims to minimize the space used to store a model and the data to be encoded in the model \cite{zhou2011structure}.

\paragraph{Minimum Description Length (MDL)}

In the case of learning Bayesian network $B$ which is composed of a graph $G$ and the associated conditional probabilities $P_B$, the MDL criterion \cite{zhou2011structure} requires choosing a network that minimizes the total description length of the network structure and the encoded data, which implies that the learning procedure balances the complexity of the induced network with the degree of accuracy with which the network represents the data.

The MDL metric is computed as:

\begin{equation}
	 MDL(G : D) = DL_{graph}(G) + \sum_i DL_{tab}(x_i) + DL_{data}(D|B)
\end{equation}


\paragraph{Graph Description Length}

Since the number of parents $|\pi(i)|$ can be encoded in $\log n$ bits, and the indices of all parents of node $i$ can be encoded in $\log(\binom{n}{\pi(i)})$  bits, the description length of the graph structure $G$ is:

\begin{equation}
	DL_{graph}(G) = \sum\limits_{i}(\log n + \log (\binom{n}{|\pi(i)|}))
\end{equation}

where $\pi(i)$ represents the set of parents of variable $x_i$.

\paragraph{CPD Description Length}

The number of parameters used for the table associated with $x_i$ is $|\pi(i)| (|x_i| - 1)$. The description length of these parameters depends on the number of bits used for each numeric parameter. A usual choice is $\frac{1}{2} \log N$ . So the description length for $x_i$’s CPD is:

\begin{equation}
	 DL_{tab}(x_i) = \frac{1}{2} |\pi(i)|(|x_i|-1) \log N
\end{equation}

\paragraph{Data Description Length}

The formula provides an information-theoretic interpretation to the representation of the data. It measures how many bits are necessary to encode the values of $x_i$ once we know $x_{\pi(i)}$.

\begin{equation}
	 DL_{data}(D|B) = - \sum\limits_i \sum\limits_{x_i, x_{\pi(i)}} \#(x_i, x_{\pi(i)}) \log P(x_i|x_{\pi(i)})
\end{equation}

where $(x_i, x_{\pi(i)})$ is a local configuration of variable $x_i$ and its parents, $\#(x_i, x_{\pi(i)})$ is the number of the occurrence of this configurationin the training data.

\subsubsection{Search methods}

Now we have a score metric to evaluate our model, we define a method to search over the model's space to reach the model which best fits to the provided data. There are many search techniques: exhaustive search, best-first search, beam search, hill climbing... These last 3 algorithms work using an heuristic function to guide the search, so we are using MDL metric defined above to reach a (local) optimum structure using Hill Climbing. If you are familiar with it, you will know that it belongs to the family of local search techniques and thus, selecting an appropriate initial state is important if we want to reach the global optimum.

\label{subsec:HC}
\subsubsection{Hill Climbing}

An easy HC algorithm \cite{zhou2011structure} can be defined as follow:

\begin{lstlisting}[mathescape=true]
	 1. We start by generating an initial graph $G$.
	 2. Then, we assign $G' = G$.
	 3. Compute all possible directed acyclic graphs (DAG) set $Neighbors(G')$,
	    adding, removing or reversing an edge in $G$.
	 4. Choose the best $neighbor$, the one with the highest score and assign
	    it to $G'$.
	 5. Go to 2 an repeat until it converges.
\end{lstlisting}



\subsection{Constrain Based Structure Learning}

\subsubsection{PC algorithm}

The PC algorithm is divided in 4 general steps \cite{colombo2014order}:

\begin{lstlisting}[mathescape=true]
	1. Adjacency search: Find the skeleton $C$ and separation sets.
	2. Orient unshielded triples in the skeleton $C$ based on the separation
	sets.
	3. In $C$ orient as many of the remaining undirected edges
	as possible by repeated application of rules R1-R3.
	4. return Output graph ($C$) and separation sets (sepset).
\end{lstlisting}

\begin{lstlisting}[mathescape=true]
	R1: orient $X_j - X_k$ into $X_ j \rightarrow X_k$ whenever there is a directed edge
	$X_i \rightarrow X_j$ such that $X_i$ and $X_k$ are not adjacent 
	(otherwise a new v-structure is created);
	R2: orient $X_i - X_j$ into $X_i \rightarrow X_j$ whenever there is a chain $X_i \rightarrow X_k \rightarrow X_j$
	(otherwise a directed cycle is created);
	R3: orient $X_i - X_j$ into $X_i \rightarrow X_j$ whenever there are two chain $X_i - X_k \rightarrow X_j$
	and $X_i - X_l \rightarrow X_j$ such that $X_k$ and $X_l$ are not adjacent (otherwise a new
	v-structure or a directed cycle is created).
\end{lstlisting}

More explanation of the steps:

\begin{enumerate}
	\item First we start with a completely connected graph. We start with $depth = 0$. We will loop increasing the depth by one each time. For every value of the depth we will investigate each pair of connected vertices.

	For every pair of vertices, we will check that the source node has a number of neighbours (without taking into account his pair that we are investigating) equal or greater than the depth

	We will make every combination of the neighbours possible, with size of the depth. Example: $S={A,B,C}\,,depth=2 \rightarrow Combinations={AB,AC,BC}$

	If the pair of connected vertices is conditionally independent given one of the combinations, we will delete that conexion from the graph, add the combination to the separation set and stop checking this pair of connected vertices.

	If the pair is dependent after having checked every combination we will take the next pair, letting the conexion as it is.
		When the depth is greater than the neighbours of every pair, we have finished the skeleton and the separation sets.

	\item The unshielded triples are 2  vertices with an undirected conexion to another vertex, but not to each other. If the connected vertex is not in the separation set of the not conected ones we can make a V structure, putting as parents the unconected ones.
	\item Explained in the rules
\end{enumerate}

\paragraph{Conditional independence}

To test the conditional independence we will use the chi squared test

\section{Parameter Learning}
Once we have the model structure, we need to learn probabilities along variables, estimating the conditional probability distribution table (CPD) of the data and model, simply computing the conditional probability of a variable conditioned by its parents.

\section{Model container}

As we are working with object oriented programming, we need to store the model as an object. We could use the python powerful library \href{https://networkx.github.io/}{Networkx}, but instead of that, we prefer implementing it from scratch for the Scored Based algorithm, as an extra. It has the next features:

\begin{itemize}
	\item Check for cycles in $O(V + E)$: since we need to work with DAG's, there must not be any cycles in the graph.
	\item Adding, removing or reversing edges.
	\item Copy model, taking into account python copy-by-reference.
	\item Saving the model visualizing as \emph{png}. It \textbf{requires} a back-end like \href{https://graphviz.gitlab.io/download/}{Graphviz}.
\end{itemize}

\section{Results}

For a complete visualization of network learning, you can access repository's \emph{README} \href{https://bitbucket.org/davidr3v1/mlnn-project-1/src/master/README.md}{here}. Also, we present some results. As we say in subsection \ref{subsec:HC} explaining the search technique, Hill Climbing needs an initial structure. That structure will be the one with a node for each variable and no edges.

\subsection{Small example: \textit{Titanic}}

\subsubsection{Structure learning}

We start before searching over model space with the network:

\begin{figure}[!htbh]
	\center\includegraphics[scale=0.28]{images/tit_model_0.png}
	\caption{\label{tit_init}Inital \emph{Titanic} structure.}
\end{figure}

After 5 iterations of search, we obtain:
\begin{figure}[!htbh]
	\center\includegraphics[scale=0.28]{images/tit_model_5.png}
	\caption{\label{tit_5}\emph{Titanic} structure after 5 iterations}
\end{figure}

Finally, after $~16$ seconds of computing and 11 iterations, we reach to the (local) optimum structure:
\begin{figure}[!htbh]
	\center\includegraphics[scale=0.3]{images/tit_best.png}
	\caption{\label{fig:tit_best}Best structure learned}
\end{figure}

\subsubsection{Parameter Learning}

Once we have the model structure, we need to learn probabilities along variables, we compute the conditional probability distribution table (CPD) over the variables. Next tables show  some of the probabilities learnt based on dataset.

\begin{table}[!htpb]
	\centering
	\begin{tabular}{|c|c|}
		\hline
		$P(sex=1)$ & $0.64904$ \\
		$P(sex=2)$ & $0.35096$ \\
		\hline
	\end{tabular}
	\caption{\emph{sex} probabilities.}

	\medskip

	\begin{tabular}{|c|c|c|c|c|}
		\hline
		$survived$ & \multicolumn{2}{|c|}{1} & \multicolumn{2}{|c|}{2} \\ \hline
		\hline
		$sex$ & $1$ & $ 2 $ & $ 1 $ & $ 2 $ \\ \hline
		\hline
		$P(age=1|survived, sex)$ & $0.32479 $ & $ 0.36697 $ & $ 0.45679 $ & $ 0.38961 $ \\
		$P(age=2|survived, sex)$ & $0.6047 $ & $ 0.59633 $ & $ 0.53086 $ & $ 0.56277 $ \\
		$P(age=3|survived, sex)$ & $0.07051 $ & $ 0.0367 $ & $ 0.01235 $ & $ 0.04762 $ \\
		\hline
	\end{tabular}
	\caption{\emph{age} probabilities.}

	\medskip
	\begin{tabular}{|c|c|c|c|c|c|c|}
		\hline
		$sex$ & \multicolumn{3}{|c|}{1} & \multicolumn{3}{|c|}{2} \\ \hline
		\hline
		$age$ & $ 1 $ & $ 2 $ & $ 3 $ & $ 1 $ & $ 2 $ & $ 3 $\\ \hline
		\hline
		$P(fare=1|age,sex)$ & $ 0.98958 $ & $ 0.98563 $ & $ 0.97297 $ & $ 0.95276 $ & $ 0.96532 $ & $ 1.0 $ \\
		$P(fare=2|age,sex)$ & $ 0.01042 $ & $ 0.00862 $ & $ 0.02703 $ & $ 0.04724 $ & $ 0.0289 $ & $ 0 $ \\
		$P(fare=3|age,sex)$ & $ 0 $ & $ 0.00575 $ & $ 0 $ & $ 0 $ & $ 0.00578 $ & $ 0 $ \\
		\hline
	\end{tabular}
	\caption{\emph{fare} probabilities.}
	\label{table:fare}
\end{table}

For example, you can check for table \ref{table:fare} manually that there no exist any sample of $fare=3$ having $sex=1$ and $age=1$, so $P(fare=3|age=1,sex=1) = 0$ as indicates.

\section{Conclusion}

The structure learning of bayesian networks is very expensive, as we can see in the results. For that reason in some cases we could assign the task of creating the structure to an expert in the field we want the Bayesian Network to be based on, and just applying the parameter learning to the structure.


%\begin{appendix}
%  \listoffigures
%  \listoftables
%\end{appendix}

\bibliography{bibliography}
\bibliographystyle{ieeetr}


\end{document}
