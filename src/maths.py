import pandas


def proba(x, v, data):
    """
    Returns the probability of variable x in the dataset of taking value v.

    Parameters
    --------
    x: str
        data column index
    v: int, str
        value of x
    data: pandas DataFrame
        Dataset
    """
    assert type(data) == pandas.DataFrame, "Dataset is not a pandas DataFrame"

    col = data[x]
    if v in col.value_counts() and len(col):
        proba = col.value_counts()[v] / len(col)
    else:
        proba = 0

    return proba


def proba_cond(x, v, parents, data):
    """
    Returns the probability of a variable x taking value v conditioned by its parents taking
    another values.

    Parameters
    --------
    data: pandas dataframe
    x: str
        data column index
    v: int, str
        value of x
    parents: list of tuples
        List of tuples (n, v), where n indicates parent's name
        and v parent's value
    data: pandas DataFrame
        Dataset
    """
    assert type(data) == pandas.DataFrame, "Dataset is not a pandas DataFrame"

    query = " and ".join([f"{pa_x} == {pa_v}" for (pa_x, pa_v) in parents])

    queried = data.query(query)
    counts = queried[x].value_counts()
    if v in counts and len(queried):
        proba_conda = counts[v] / len(queried)
    else:
        proba_conda = 0

    return proba_conda
