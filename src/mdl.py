import math

import numpy
import pandas
from scipy.special import comb

from Model import Model


class MDL:
    def __init__(self, data):
        """
        Minimum Description Length implementation.

        The Minimum Description Length (MDL) principle [Rissanen, 1989] aims to minimize the space
        used to store a model and the data to be encoded in the model. In the case of learning Bayesian
        network B which is composed of a graph G and the associated conditional probabilities P_B,
        the MDL criterion requires choosing a network that minimizes the total description length of
        the network structure and the encoded data, which implies that the learning procedure balances
        the complexity of the induced network with the degree of accuracy with which the network represents
        the data.

        Parameters
        ---------
        data: pandas.DataFrame
            Dataset

        References
        ---------
        .. [1] Structure Learning of Probabilistic Graphical Models:  A Comprehensive Survey,  Y. Zhou, Chapter 3.1.1, https://arxiv.org/pdf/1111.6925.pdf
        .. [2] Jorma Rissanen. Stochastic Complexity in Statistical Inquiry Theory. World Scientific Publishing Co., Inc., River Edge, NJ, USA, 1989. ISBN 981020311X.
        """
        assert type(data) == pandas.DataFrame
        self.data = data

        self.epsilon = 0

        self._times_in_ds = {}
        self._combinations = {}
        self._proba = {}
        self._proba_cond = {}
        self._occurrences = {}

    def proba(self, x, v):
        """
        Returns the probability of variable x in the dataset of taking value v.

        Parameters
        --------
        x: str
            data column index
        v: int, str?
            value of x
        """
        if (x, v) not in self._proba:
            col = self.data[x]
            if v in col.value_counts() and len(col):
                self._proba[(x, v)] = col.value_counts()[v] / len(col)
            else:
                self._proba[(x, v)] = self.epsilon

        return self._proba[(x, v)]

    def proba_cond(self, x, v, parents):
        """
        Returns the probability of a variable x taking value v conditioned by its parents taking
        another values.

        Parameters
        --------
        data: pandas dataframe
        x: str
            data column index
        v: int, str?
            value of x
        parents: list of tuples
            List of tuples (n, v), where n indicates parent's name
            and v parent's value

        Examples
        --------
        >>> parents = [("B", 0), ("A", 1)]
        """
        query = " and ".join([f"{pa_x} == {pa_v}" for (pa_x, pa_v) in parents])

        if (x, v, query) not in self._proba_cond:
            queried = self.data.query(query)
            counts = queried[x].value_counts()
            if v in counts and len(queried):
                self._proba_cond[(x, v, query)] = counts[v] / len(queried)
            else:
                self._proba_cond[(x, v, query)] = self.epsilon

        return self._proba_cond[(x, v, query)]

    @staticmethod
    def dl_graph(model):
        """
        Computes Description Length of the graph.

        Since the number of parents :math:`|\pi(i)|` can be encoded in :math:`\log n` bits, and the
        indices of all parents of node `i` can be encoded in :math:`\log(n \pi(i))`  bits, the description
        length of the graph structure `G` is [1]_:

        .. math:: DL_{GRAPH}(G) = \sum_{i}(\log n + \log ( \\binom{n}{|\pi(i)|} ))

        Parameters
        ---------
        model: dict, Model
            Model structure

        Returns
        -------
        int
            Description Length score of graph `G`.

        References
        ---------
        .. [1] Y. Zhou, Structure Learning of Probabilistic Graphical Models:  A Comprehensive Survey,
            Chapter 3.1.1, equation 3.1, https://arxiv.org/pdf/1111.6925.pdf

        See also
        --------
        - Scipy Comb: https://docs.scipy.org/doc/scipy/reference/generated/scipy.special.comb.html

        Examples
        ------
        >>> df = pandas.DataFrame([[0, 0, 1], [1, 0, 0], [0, 0, 1], [1, 1, 0], [0, 1, 0], [1, 1, 1]], columns=list("ABC"))
        >>> mdl = MDL(df)
        >>> model = Model({"A":{"B", "C"}, "B":{"C"}, "C":{})
        >>> mdl.dl_graph(model)
        5.493061443340549
        """
        assert type(model) in [dict, Model], "Model should be passed as dict or Model instance"
        if type(model) is dict:
            model = Model(model)

        # Number of variables in the model
        n = len(model.nodes)

        s = 0
        for (_, pa) in model.parents.items():
            s += math.log(n) + math.log(comb(n, len(pa)))

        return s

    def dl_tab(self, model, x):
        """
        Computes Description Length over the CPD table.

        The number of parameters used for the table associated with `x_i` is
        :math:`|\\pi(i)|(|x_i|−1)`. The description length of these parameters depends on
        the number of bits used for each numeric parameter. A usual choice is `1/2 logN` .
        So the description length for :`x_i`’s CPD is:

        .. math:: DL_{TAB}(x_i) = 0.5 * |\\pi(i)|(|x_i|−1) \\log N

        Parameters
        ---------
        model: dict, Model
            Model structure
        x: str
            Variable name

        Returns
        -------
        int
            Description Length score of `x_i` CPD.

        References
        ---------
        .. [1] Y. Zhou, Structure Learning of Probabilistic Graphical Models:  A Comprehensive Survey,
            Chapter 3.1.1, https://arxiv.org/pdf/1111.6925.pdf
        """
        assert type(model) in [dict, Model], "Model should be passed as dict or Model instance"
        if type(model) is dict:
            model = Model(model)

        return 0.5 * len(model.parents[x]) * (len(self.data[x].value_counts().keys()) - 1) * math.log(
            len(self.data))

    def _times_in_dataset(self, x, val, pa, pa_values):
        """
        Counts the number of times the combinations (x=val, pa=pa_values)
        appears in the dataset.

        Parameters
        ---------
        x: str
            Variable name in dataset
        val: int, float
            Variable value
        pa: tuple
            Parent's names in dataset
        pa_values: list
            Parent's values

        Returns
        -------
        int
            Occurrences of given combination

        References
        ---------
        - Pandas Query, https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.query.html
        """
        query = f"{x} == {val} and "
        query += " and ".join([f"{pa[i]} == {pa_values[i]}" for i in range(len(pa))])

        if query not in self._times_in_ds:
            queried = self.data.query(query)
            self._times_in_ds[query] = len(queried)

        return self._times_in_ds[query]

    def _combinations_of_parents_values(self, parents):
        """
        Generates all possible combinations of parent's values, taking
        into account that parents may not have same values.

        Parameters
        ----------
        parents: tuple
            Parent's variable names in the dataset

        Returns
        -------
        ndarray
            Combinations of values

        References
        --------
        - NumPy Tile: https://docs.scipy.org/doc/numpy/reference/generated/numpy.tile.html
        - NumPy Repeat: https://docs.scipy.org/doc/numpy/reference/generated/numpy.repeat.html
        """

        if parents not in self._combinations:
            values = {p: list(self.data[p].value_counts().keys()) for p in parents}
            i = 0
            num_combinations = numpy.product([len(v) for v in values.values()])
            combinations = numpy.zeros((num_combinations, len(values.keys())))
            r = 1
            while i < len(values.keys()):
                parent = list(values.keys())[i]
                l = numpy.tile(numpy.repeat(values[parent], r), num_combinations // len(values[parent]))
                combinations[:, i] = l[:num_combinations]
                r *= len(values[parent])
                i += 1

            self._combinations[parents] = combinations

        return self._combinations[parents]

    def dl_data(self, model):
        """
        Computes the Description Length over the train data.

        The formula [1]_ provides an information-theoretic interpretation to the representation of the data:
        it measures how many bits are necessary to encode the values ofxionce we know `xπ(i)`.

        Parameters
        ---------
        model: dict, Model
            Model structure

        Returns
        -------
        int
            Description Length score of dataset

        References
        ---------
        .. [1] Y. Zhou, Structure Learning of Probabilistic Graphical Models:  A Comprehensive Survey,
            Chapter 3.1.1, https://arxiv.org/pdf/1111.6925.pdf
        """
        assert type(model) in [dict, Model], "Model should be passed as dict or Model instance"
        if type(model) is dict:
            model = Model(model)

        s = 0
        for x in model.nodes:
            x_values = list(self.data[x].value_counts().keys())
            pa = tuple(model.parents[x])

            if pa:
                pa_values = self._combinations_of_parents_values(pa)
                for x_value in x_values:
                    for pa_value in pa_values:
                        occurences = self._times_in_dataset(x, x_value, pa, pa_value)

                        if occurences:
                            prob_cond = self.proba_cond(x, x_value, zip(pa, pa_value))
                            s += occurences * math.log(prob_cond)

            else:
                for x_value in x_values:
                    if (x, x_value) not in self._occurrences:
                        self._occurrences[(x, x_value)] = self.data[x].value_counts()[x_value]

                    prob = self.proba(x, x_value)

                    s += self._occurrences[(x, x_value)] * math.log(prob)

        s *= -1

        return s

    def score(self, model):
        """
        Computes the full Minimum Description Length score.

        Parameters
        ---------
        model: dict, Model
            Model structure

        Returns
        -------
        int
            Description Length score of graph `G` and data `D`.

        References
        ---------
        .. [1] Y. Zhou, Structure Learning of Probabilistic Graphical Models:  A Comprehensive Survey,
            Chapter 3.1.1, equation 3.2, https://arxiv.org/pdf/1111.6925.pdf
        """
        assert type(model) in [dict, Model], "Model should be passed as dict or Model instance"
        if type(model) is dict:
            model = Model(model)

        dl_graph = self.dl_graph(model)
        dl_tab = sum([self.dl_tab(model, x) for x in model.nodes])
        dl_data = self.dl_data(model)

        return dl_graph + dl_tab + dl_data


if __name__ == '__main__':
    df = pandas.DataFrame([[0, 0, 1], [1, 0, 0], [0, 0, 1], [1, 1, 0], [0, 1, 0], [1, 1, 1]], columns=list("ABC"))
    model = Model({"A": {"B", "C"}, "B": {"C"}})
    mdl = MDL(df)

    print(MDL.dl_graph(model))
    print(mdl.dl_tab(model, "C"))
    print(mdl.dl_data(model))

    print(mdl.score(model))

    help(mdl.dl_graph)
