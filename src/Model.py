from copy import deepcopy

import graphviz
import numpy
from graphviz import Digraph


class Model:
    def __init__(self, graph):
        """
        Graph network implementation

        Parameters
        ---------
        graph: python dict
            A dictionary containing the graph, where keys are nodes
            and values are a list containing children nodes.

        Examples
        --------
        >>> model = Model({"A": {"C"}, "B": {"C"}, "C": {}})  # B -> C <- A
        >>> model = Model({"A": {"B", "C"}})  # B <- A -> C
        """
        g = graph.copy()
        for (v, e) in g.items():
            for edge in e:
                if edge not in g.keys():
                    graph[edge] = set()

        # Check if is cyclic
        assert not Model.is_cyclic(graph), "The graph must not contain cycles"

        self.nodes = graph.keys()
        self.children = graph
        self.parents = Model._parents(graph)

        self.node_configs = {}
        self._parents_combinations = {}

    def __str__(self):
        return str(self.children)

    def __repr__(self):
        return str(self.children)

    def __eq__(self, other):
        return other.children == self.children

    def __copy__(self):
        model_copy = Model(deepcopy(self.children))
        return model_copy

    @staticmethod
    def is_cyclic(graph):
        """
        Return True if the directed graph has a cycle.
        The graph must be represented as a dictionary mapping vertices to
        iterables of neighbouring vertices.

        Runtime of O(V + E)

        References
        ----------
        - https://codereview.stackexchange.com/a/86067

        Examples
        --------
        >>> is_cyclic({1: {2}, 2: {3}, 3: {1,}})
        True
        >>> is_cyclic({1: {2}, 2: {3}, 3: {4,}})
        False
        """
        assert type(graph) == dict, "Graph must be dict type"

        visited = set()
        path = [object()]
        path_set = set(path)
        stack = [iter(graph)]
        while stack:
            for v in stack[-1]:
                if v in path_set:
                    return True
                elif v not in visited:
                    visited.add(v)
                    path.append(v)
                    path_set.add(v)
                    stack.append(iter(graph.get(v, ())))
                    break
            else:
                path_set.remove(path.pop())
                stack.pop()
        return False

    @staticmethod
    def _parents(g):
        """
        Returns parents of each node

        Parameters
        --------
        g: python dict
            A dictionary representing the graph.

        Returns
        -------
        dict

        Examples
        -------
        >>> model = Model({"A": {"C"}, "B": {"C"}, "C": {}})  # B -> C <- A
        >>> model.parents
        {"A": set(), "B": set(), "C": {"A", "B"}}
        """
        parents = {k: set() for k in g.keys()}
        for (node, children) in g.items():
            for child in children:
                parents[child].add(node)

        return parents

    def add_edge(self, o, d):
        """
        Adds an edge between nodes `o` and `d`

        Parameters
        ----------
        o, d: str
            Node name
        """
        if o in self.children.keys() and d in self.nodes:
            self.children[o].add(d)
            self.parents[d].add(o)

            # Check for cycle
            if Model.is_cyclic(self.children):
                self.children[o].remove(d)
                self.parents[d].remove(o)

    def remove_edge(self, o, d):
        """
        Removes an existing edge between nodes `o` and `d`

        Parameters
        ----------
        o, d: str
            Node name
        """
        if o in self.nodes and d in self.parents.keys():
            if d in self.children[o]:
                self.children[o].remove(d)
                self.parents[d].remove(o)

                # Check for cycle
                if Model.is_cyclic(self.children):
                    self.children[o].remove(d)
                    self.parents[d].remove(o)

    def reverse_edge(self, o, d):
        """
        Removes an existing edge between nodes `o` and `d`

        Parameters
        ----------
        o, d: str
            Node name
        """
        if o in self.nodes and d in self.parents.keys():
            if d in self.children[o]:
                self.remove_edge(o, d)
                self.add_edge(d, o)

    def copy(self):
        return self.__copy__()

    def plot(self, fig_name):
        """Plots the model"""
        try:
            dot = Digraph()
            for n in self.nodes:
                dot.node(n, n)

            for (parent, children) in self.children.items():
                for child in children:
                    dot.edge(parent, child)

            # For saving png
            dot.render(fig_name.replace(' ', '_'), view=False, format="png")
            # For Jupyter displaying (from IPython.display import display)
            # display(dot)

        except graphviz.backend.ExecutableNotFound:
            print("[WARNING] Can't visualize model, install a backend like Graphviz or so.")

    def node_configurations(self, node, data):
        """
        Returns all combinations of possible configuration values of the
        given variable with the given parents of the model.

        Parameters
        ----------
        node: int, str
            node's variable names in the dataset
        data: pandas DataFrame
            Dataset
        """
        if node not in self.node_configs:
            self.node_configs[node] = numpy.array(data[node].value_counts().keys())

        return self.node_configs[node]

    def parents_configurations(self, parents, data):
        """
        Generates all possible combinations of parent's values, taking
        into account that parents may not have same values.

        Parameters
        ----------
        parents: tuple
            parents's variable names in the dataset
        data: pandas DataFrame
            Dataset

        Returns
        -------
        ndarray
            Combinations of values

        References
        --------
        - NumPy Tile: https://docs.scipy.org/doc/numpy/reference/generated/numpy.tile.html
        - NumPy Repeat: https://docs.scipy.org/doc/numpy/reference/generated/numpy.repeat.html
        """

        if parents not in self._parents_combinations:
            values = {p: list(data[p].value_counts().keys()) for p in parents}
            i = 0
            num_combinations = numpy.product([len(v) for v in values.values()])
            combinations = numpy.zeros((num_combinations, len(values.keys())))
            r = 1
            while i < len(values.keys()):
                parent = list(values.keys())[i]
                l = numpy.tile(numpy.repeat(values[parent], r), num_combinations // len(values[parent]))
                combinations[:, i] = l[:num_combinations]
                r *= len(values[parent])
                i += 1

            self._parents_combinations[parents] = combinations

        return self._parents_combinations[parents]
