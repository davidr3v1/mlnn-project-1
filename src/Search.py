import logging
import time
from itertools import permutations

import numpy
import pandas

from Model import Model
from mdl import MDL


class HillClimbing:
    def __init__(self, max_iter=100, level=logging.INFO):
        """
        Hill Climbing search method implementation.

        Parameters
        ----------
        max_iter: int
            Maximum iterations

        References
        ---------
        - Articial Intelligence: A Modern Approach, Russel & Norvig (2nd ed.)
        - Structure Learning of Probabilistic Graphical Models:  A Comprehensive Survey,  Y. Zhou, Chapter 3.2, https://arxiv.org/pdf/1111.6925.pdf
        """
        self.epsilon = 10e-10
        self.blocked_states = []
        self.max_iter = max_iter

        logging.basicConfig(format='%(message)s', level=level)

    def neighbors(self, model):
        """
        Returns
        -------
        list
            list of Model instances
        """
        combs = list(permutations(model.nodes, 2))
        self.blocked_states.append(model)

        # Add edges
        added = []
        for (o, d) in combs:
            m = model.copy()
            m.add_edge(o, d)
            added.append(m)

        # Remove edges
        removed = []
        for (o, d) in combs:
            m = model.copy()
            m.remove_edge(o, d)
            if m not in self.blocked_states:
                removed.append(m)

        # Reversed edges
        reverse = []
        for (o, d) in combs:
            m = model.copy()
            m.reverse_edge(o, d)
            if m not in self.blocked_states:
                reverse.append(m)

        neighbors = added + removed + reverse

        return neighbors

    def search(self, data, save=False):
        """
        Parameters
        ----------
        data: pandas.Dataframe
            Dataset
        save: bool
            Save the intermediate models

        References
        ----------
        - Structure Learning of Probabilistic Graphical Models:  A Comprehensive Survey,  Y. Zhou, Algorithm 5, https://arxiv.org/pdf/1111.6925.pdf
        """
        t = time.time()
        assert type(data) == pandas.DataFrame, "Dataset is not a pandas DataFrame"

        # Define a score metric
        metric = MDL(data)

        # Initialize a structure G'
        final = Model({node: set() for node in data.keys()})
        final.plot("model_0")
        logging.info(f"Starting Hill Climb algorithm, {len(final.nodes)} variables detected")
        logging.info("Initializing empty model.")

        converged = False
        last_score = -float('inf')
        i = 0
        while not converged and i < self.max_iter:
            model = final.copy()
            logging.info(f"[iter:{i}] Computing neighbors.")
            neighbors = self.neighbors(model)

            logging.info(f"[iter:{i}] Computing scores")
            scores = numpy.array([metric.score(n) for n in neighbors])
            best_index = int(numpy.argmax(scores))

            score = scores[best_index]
            final = neighbors[best_index].copy()
            logging.info(f"[iter:{i}] Best score: {score}")

            # Check convergence
            if abs(last_score - score) >= self.epsilon:
                last_score = score
            else:
                converged = True

            i += 1

            # Plot graph
            if save:
                final.plot(f"model_{i}")

        final.plot("local_best")
        logging.info(f"Hill Climb took {round(time.time() - t, 3)}s and {i} iterations.")
        return final
