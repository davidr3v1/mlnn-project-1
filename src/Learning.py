import numpy
import pandas

import maths
from Model import Model


class ParameterEstimation:
    def __init__(self, model, data):
        """
        Parameter estimation por model variables
        """
        assert type(data) == pandas.DataFrame, "Dataset is not a pandas DataFrame"
        assert isinstance(model, Model), "Model is not a model object"

        # Pandas dataframe
        self.data = data

        self.epsilon = 0

        self.model = model
        self._occurrences = {}
        self.cpd = {}
        self._proba = {}
        self._proba_cond = {}
        self._times_in_ds = {}

    def _times_in_dataset(self, x, val, pa, pa_values):
        """
        Counts the number of times the combinations (x=val, pa=pa_values)
        appears in the dataset.

        Parameters
        ---------
        x: str
            Variable name in dataset
        val: int, float
            Variable value
        pa: tuple
            Parent's names in dataset
        pa_values: list
            Parent's values

        Returns
        -------
        int
            Occurrences of given combination

        References
        ---------
        - Pandas Query, https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.query.html
        """
        query = f"{x} == {val} and "
        query += " and ".join([f"{pa[i]} == {pa_values[i]}" for i in range(len(pa))])

        if query not in self._times_in_ds:
            queried = self.data.query(query)
            self._times_in_ds[query] = len(queried)

        return self._times_in_ds[query]

    def estimate_cpd(self, node, show=True):
        """
        Estimate the CPD for a given variable.

        Parameters
        ----------
        node: int, string
            The name of the variable for which the CPD is to be estimated.
        show: bool
            Plot CPD table

        Returns
        -------
        dict
        """

        if node not in self.cpd:
            pa = tuple(self.model.parents[node])
            x_values = self.model.node_configurations(node, self.data)
            self.cpd[node] = {x_value: {} for x_value in x_values}

            if pa:
                pa_values = self.model.parents_configurations(pa, self.data)
                for x_value in x_values:
                    for pa_value in pa_values:
                        occurences = self._times_in_dataset(node, x_value, pa, pa_value)

                        zipped_pa = tuple(zip(pa, pa_value))
                        query = " and ".join([f"{pa_x} == {pa_v}" for (pa_x, pa_v) in zipped_pa])
                        if occurences:
                            if (node, x_value, query) not in self._proba_cond:
                                self._proba_cond[(node, x_value, query)] = maths.proba_cond(node, x_value,
                                                                                            zipped_pa, self.data)
                        else:
                            self._proba_cond[(node, x_value, query)] = 0

                        # Add to CPD
                        self.cpd[node][x_value][zipped_pa] = self._proba_cond[(node, x_value, query)]

            else:
                for x_value in x_values:
                    if (node, x_value) not in self._occurrences:
                        self._occurrences[(node, x_value)] = self.data[node].value_counts()[x_value]

                    if self._occurrences[(node, x_value)] and (node, x_value) not in self._proba:
                        self._proba[(node, x_value)] = maths.proba(node, x_value, self.data)
                    else:
                        self._proba[(node, x_value)] = 0

                    # Add to CPD
                    self.cpd[node][x_value] = self._proba[(node, x_value)]

        if show:
            self.show_table(node)

        return self.cpd[node]

    def show_table(self, node):
        # FIXME: Table headers are bad formatted
        parents = tuple(self.model.parents[node])
        table = []
        cell_len = 10
        if parents:
            cell_len *= len(parents)
            row = 1
            for parent in parents[::-1]:
                parent_states = sorted(numpy.unique(self.data[parent].values))
                h = ""
                for _ in range(row):
                    for state in map(str, parent_states):
                        h += f"{state:^{cell_len - 1}}|"
                table.append(f'| {parent:^15.13} |{h}')

                cell_len = cell_len // len(parents)
                row += 1

            cell_len = 10
            table.append(f'| {node:^15.13} |')

            for key in self.cpd[node].keys():
                dictionary = list(self.cpd[node][key].items())
                items = [(tuple(map(lambda x: x[1], d[0])), d[1]) for d in dictionary]
                items.sort(key=lambda x: x[0])

                probs = list(map(str, [round(item[1], 5) for item in items]))
                r = ""
                for prob in probs:
                    r += f"{prob:^{cell_len - 1}}|"
                table.append(f'| {str(key):^15} |{r}')

        else:
            table.append(f'| {node:^15.13}')
            items = list(self.cpd[node].items())
            items.sort(key=lambda x: x[0])
            for (key, prob) in items:
                prob = round(prob, 5)
                r = f"{prob:^{cell_len - 1}}|"
                table.append(f'| {key:^15} |{r}')

        print("\n".join(table), end="\n\n")

    def parameters(self, show=True):
        """
        Returns model parameters

        Parameters
        ----------
        show: bool
            Plot CPD table

        Returns
        -------
        dict
        """
        parameters = {node: self.estimate_cpd(node, show=show) for node in self.model.nodes}

        return parameters


def test():
    data = [[0, 1, 0],
            [1, 1, 1],
            [0, 0, 1],
            [1, 1, 0],
            [0, 0, 0],
            [1, 0, 0]]

    df = pandas.DataFrame(data, columns=["A", "B", "C"])
    model = Model({"A": {"C"}, "B": {"C"}, "C": {}})  # B -> C <- A

    pe = ParameterEstimation(model, df)
    pe.parameters()


if __name__ == '__main__':
    test()
