import unittest
from unittest import TestCase

from src.Model import Model


class TestModel(TestCase):
    def setUp(self):
        self.model1 = Model({"A": {"C"}, "B": {"C"}, "C": set()})  # B -> C <- A    
        self.model2 = Model({"A": {"B", "C"}})  # B <- A -> C

    def test_add_edge(self):
        # Adding edges between existing nodes
        self.model2.add_edge("B", "C")  # C <- B <- A -> C
        self.assertEqual(self.model2.parents, {"A": set(), "B": {"A"}, "C": {"A", "B"}})
        self.assertEqual(self.model2.children, {"A": {"B", "C"}, "B": {"C"}, "C": set()})

        # Adding edges between non-existing nodes
        self.model2.add_edge("B", "D")  # C <- B <- A -> C
        self.assertEqual(self.model2.parents, {"A": set(), "B": {"A"}, "C": {"A", "B"}})
        self.assertEqual(self.model2.children, {"A": {"B", "C"}, "B": {"C"}, "C": set()})

        self.model2.add_edge("D", "B")  # ? -> B <- A -> C
        self.assertEqual(self.model2.parents, {"A": set(), "B": {"A"}, "C": {"A", "B"}})
        self.assertEqual(self.model2.children, {"A": {"B", "C"}, "B": {"C"}, "C": set()})

    def test_remove_edge(self):
        # Removing existing edges between existing nodes
        self.model2.remove_edge("B", "C")  # C <- B <- A -> C
        self.assertEqual(self.model2.parents, {"A": set(), "B": {"A"}, "C": {"A"}})
        self.assertEqual(self.model2.children, {"A": {"B", "C"}, "B": set(), "C": set()})

        # Removing existing edges between existing nodes
        self.model2.remove_edge("B", "C")  # B <- A -> C
        self.assertEqual(self.model2.parents, {"A": set(), "B": {"A"}, "C": {"A"}})
        self.assertEqual(self.model2.children, {"A": {"B", "C"}, "B": set(), "C": set()})

        # Removing non-existing edges between (non-existing) nodes
        self.model2.remove_edge("B", "A")  # B <- A -> C
        self.assertEqual(self.model2.parents, {"A": set(), "B": {"A"}, "C": {"A"}})
        self.assertEqual(self.model2.children, {"A": {"B", "C"}, "B": set(), "C": set()})

        self.model2.remove_edge("B", "D")  # B <- A -> C
        self.assertEqual(self.model2.parents, {"A": set(), "B": {"A"}, "C": {"A"}})
        self.assertEqual(self.model2.children, {"A": {"B", "C"}, "B": set(), "C": set()})

    def test_reverse_edge(self):
        # Reversing existing edges between existing nodes
        self.model2.reverse_edge("A", "B")  # B -> A -> C
        self.assertEqual(self.model2.parents, {"A": {"B"}, "B": set(), "C": {"A"}})
        self.assertEqual(self.model2.children, {"A": {"C"}, "B": {"A"}, "C": set()})

        # Reversing non-existing edges between (non-existing) nodes
        self.model2.reverse_edge("A", "B")  # B -> A -> C
        self.assertEqual(self.model2.parents, {"A": {"B"}, "B": set(), "C": {"A"}})
        self.assertEqual(self.model2.children, {"A": {"C"}, "B": {"A"}, "C": set()})

        self.model2.reverse_edge("A", "D")  # B -> A -> C
        self.assertEqual(self.model2.parents, {"A": {"B"}, "B": set(), "C": {"A"}})
        self.assertEqual(self.model2.children, {"A": {"C"}, "B": {"A"}, "C": set()})

    def test_plot(self):
        self.model1.plot("prueba1")
        self.model2.plot("prueba2")


if __name__ == '__main__':
    unittest.main()
