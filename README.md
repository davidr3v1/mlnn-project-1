# Bayesian Network Structure Learner

## Installation

Just install requirements with `pip install --user -r requirements.txt` on your system or virtual environment.

## Running
There is no entry point, just have fun with `HillClimbing.search()` method. All iterations output an image of
the actual graph, so a backend like [`graphviz`](https://graphviz.gitlab.io/download/) should be installed,
but is optional.

## Score-based structure learning
### Score metric

There exist many score metrics available. We decide to use the Minimum Description Length, which  aims
to minimize the space used to store a model and the data to be encoded in the model [1].

### Search
1. For this type of learning, we start by generating an initial graph `G`. Despite of being possible using
another search algorithms, we use Hill Climb method. If you are familiar with it, you will know
that it belongs to the family of local search techniques and thus, selecting an appropriate initial state
is important if we want to reach the global optimum. Well, we don't take that into account.

2. Then, we assign `G' = G`.
3. Compute all possible directed acyclic graphs (DAG) set `Neighbors` from `G'`, adding, removing or reversing
an edge in `G`.
4. Choose the best *neighbor*, the one with the highest score and assign it to `G'`
5. Go to 2 an repeat until it converges.

### Examples
#### Small CSV - Titanic
This data set provides information on the fate of passengers on the fatal maiden voyage of
the ocean liner *Titanic*, summarized according to economic status (class), sex, age and
survival [2]. Stanford has discretized the data so that you would only have to deal with
discrete variables [3].
##### Execution
![Empty model](examples/small/model_0.png)

After computing neighbors' scores, the best achieve *4140,11*.

![model_1](examples/small/model_1.png)

After computing neighbors' scores, the best achieve *4147,82*.

![model_2](examples/small/model_2.png)

After computing neighbors' scores, the best achieve *4154,61*.

![model_3](examples/small/model_3.png)

After computing neighbors' scores, the best achieve *4160,48*.

![model_4](examples/small/model_4.png)

After computing neighbors' scores, the best achieve *4165,79*.

![model_5](examples/small/model_5.png)

After computing neighbors' scores, the best achieve *4170,0*.

![model_6](examples/small/model_6.png)

After computing neighbors' scores, the best achieve *4174,00*.

![model_7](examples/small/model_7.png)

After computing neighbors' scores, the best achieve *4177,21*.

![model_8](examples/small/model_8.png)

After computing neighbors' scores, the best achieve *4179,54*.

![model_9](examples/small/model_9.png)

After computing neighbors' scores, the best achieve *4181,51*.

![local_best](examples/small/local_best.png)

After computing neighbors' scores, none of them improved. It took 15s and 11 iterations.

#### Medium CSV - Wine
This data set provides information related to red and white variants of
the Portuguese "Vinho Verde" wine [4].
##### Execution
![Empty model](examples/medium/model_0.png)

After computing neighbors' scores, the best achieve *45199,21*.

![model_1](examples/medium/model_1.png)

After computing neighbors' scores, the best achieve *45212.03*.

![model_2](examples/medium/model_2.png)

After computing neighbors' scores, the best achieve *45224.7*.

![model_3](examples/medium/model_3.png)

After computing neighbors' scores, the best achieve *45237.25*.

![model_4](examples/medium/model_4.png)

After computing neighbors' scores, the best achieve *45248.22*.

![model_5](examples/medium/model_5.png)

After computing neighbors' scores, the best achieve *45257.76*.

![model_6](examples/medium/model_6.png)

After computing neighbors' scores, the best achieve *45266.26*.

![model_7](examples/medium/model_7.png)

After computing neighbors' scores, the best achieve *45273.26*.

![model_8](examples/medium/model_8.png)

After computing neighbors' scores, the best achieve *45279.48*.

![model_9](examples/medium/model_9.png)

After computing neighbors' scores, the best achieve *45283.29*.

![model_10](examples/medium/model_10.png)

After computing neighbors' scores, the best achieve *45285.43*.

![model_11](examples/medium/model_11.png)

After computing neighbors' scores, the best achieve *45286.41*.

![model_12](examples/medium/model_12.png)

After computing neighbors' scores, the best achieve *45286.4116*.

![model_13](examples/medium/model_13.png)

After computing neighbors' scores, the best achieve *4586.4157*.

![local_best](examples/medium/local_best.png)

After computing neighbors' scores, none of them improved. It took 106s and 14 iterations.

#### Insane Example
##### Execution
![Empty model](examples/large/images/model_0.png)

This was the initial model. After 20 iterations and almost 1h later:

![model_20](examples/large/images/model_20.png)

After 3 hours later, the iteration 81 gets:

![model_81](examples/large/images/model_81.png)

It did not converge...


## Conditional independence tests

## References
- [1] [Structure Learning of Probabilistic Graphical Models:  A Comprehensive Survey,  Y. Zhou](https://arxiv.org/pdf/1111.6925.pdf)
- [2] https://cran.r-project.org/web/packages/titanic/titanic.pdf
- [3] https://web.stanford.edu/class/aa228/projects/1/
- [4] https://archive.ics.uci.edu/ml/datasets/Wine+Quality

